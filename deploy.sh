#!/bin/bash
set -euo pipefail

cp -R gagallium-output/* gagallium-save/
cd gagallium-save
git add .
git commit -m "deploy again: $1"
git push
cd ..
scp -r gagallium-output/* yquem.inria.fr:/net/yquem/infosystems/www/gallium/gallium/blog/
