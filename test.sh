#!/bin/bash
set -euo pipefail

ssh yquem.inria.fr rm -rf public_html/gagallium-test
scp -p -r gagallium-test yquem.inria.fr:public_html
