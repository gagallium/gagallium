#!/bin/bash
set -euo pipefail

# get the article sources
git clone git@gitlab.inria.fr:gagallium/gagallium-source.git

# get a copy of the backup repository
git clone git@gitlab.inria.fr:gagallium/gagallium-save.git

# create the directory for test renderings
mkdir gagallium-test

# create the directory for final renderings
mkdir gagallium-output
